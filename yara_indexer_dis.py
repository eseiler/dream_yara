#!/usr/bin/python3

import argparse, os, sys, subprocess, io, time, shlex, shutil, gzip

def main():
	version = '1.0'

	default_parser = argparse.ArgumentParser(add_help=False)
	default_group = default_parser.add_argument_group('General parameters')
	default_group.add_argument('-i', '--input-files', required=True, nargs="*", type=str, help='Multi-fasta[.gz] file[s] to create or add to existing indices')
	default_group.add_argument('-o', '--output-folder', type=str, required=True, help='Folder where the indices are created. If updating the information is loaded from here.')
	default_group.add_argument('-t', '--threads', type=int, default=1, help='set the number of subprocesses/threads to use for calculations (default: 1)')
	default_group.add_argument('--taxsbp-path', type=str, default='./', help='TaxSBP script path. Default: ./')
	default_group.add_argument('--yara-path', type=str, default='./', help='Yara binaries path. Default: ./')
	
	default_create_parser = argparse.ArgumentParser(add_help=False)
	default_create_group = default_create_parser.add_argument_group('General parameters')
	default_create_group.add_argument('-b', '--number-of-bins', type=int, default=64, help='Approximate number of bins (indices) in range [1..10000] [Mutually exclusive --bin-length] (default: 64)')
	default_create_group.add_argument('-z', '--gzip-output', default=False, action='store_true', help='Gzip the output fasta files')
	default_update_parser = argparse.ArgumentParser(add_help=False)
	default_update_group = default_update_parser.add_argument_group('General parameters')
	default_update_group.add_argument('--remove-file', type=str, help='File with a list of sequence ids to remove from the indices')

	taxsbp_parser = argparse.ArgumentParser(add_help=False)
	taxspb_group = taxsbp_parser.add_argument_group('Taxonomic clustering (TaxSBP) parameters')
	taxspb_group.add_argument('--retrieve-ncbi', default=False, action='store_true', help='Retrieve taxdump (--nodes-file, --merged-file) and TaxSBP information (--len-taxid-file) automatically from NCBI')
	taxspb_group.add_argument('--len-taxid-file', type=str, help='Tab-separated file with the sequence ids, sequence length and taxonomic id [Mutually exclusive --retrieve-ncbi]')
	taxspb_group.add_argument('--nodes-file', type=str, help='nodes.dmp from NCBI Taxonomy [Mutually exclusive --retrieve-ncbi]')
	taxspb_group.add_argument('--merged-file', type=str, help='merged.dmp from NCBI Taxonomy [Mutually exclusive --retrieve-ncbi]')

	taxsbp_create_parser = argparse.ArgumentParser(add_help=False)
	taxspb_create_group = taxsbp_create_parser.add_argument_group('Taxonomic clustering (TaxSBP) parameters')
	taxspb_create_group.add_argument('--start-node', type=int, default=1, help='Start node taxonomic id. Default: 1 (root node)')
	taxspb_create_group.add_argument('--bin-length', type=int, help='Maximum bin length (in bp). Use this parameter to define the number of bins [Mutually exclusive --number-of-bins]')
	taxsbp_update_parser = argparse.ArgumentParser(add_help=False)
	taxsbp_update_group = taxsbp_update_parser.add_argument_group('Taxonomic clustering (TaxSBP) parameters')
	taxsbp_update_group.add_argument('--distribute', default=False, action='store_true', help='Distribute newly added sequences among more bins, evenly distributing the sequences. Without this option, fewer bins will be updated.')

	yara_indexer_create_parser = argparse.ArgumentParser(add_help=False)
	yara_indexer_create_group = yara_indexer_create_parser.add_argument_group('Yara (yara_build_filter) parameters')
	yara_indexer_create_group.add_argument('-k', '--kmer-size', type=int, default=19, help='The k-mer size for the bloom filter in range [14..32] (default: 19)')
	yara_indexer_create_group.add_argument('-nh', '--num-hash', type=int, default=4, help='The number of hash functions to use for the bloom filter in range [2..5] (default: 4)')
	yara_indexer_create_group.add_argument('-bs', '--bloom-size', type=int, default=1, help='Expected bloom filter size in GB. [1..512] (default: 1)')
	
	parser = argparse.ArgumentParser(prog='yara_indexer_dist', description='yara_indexer_dist', conflict_handler='resolve')
	parser.add_argument('-v', '--version', action='version', version='%(prog)s ' + version)
	subparsers = parser.add_subparsers()

	# create
	create_parser = subparsers.add_parser('create', help='Create new clusters and index', parents=[default_parser,default_create_parser,taxsbp_parser,taxsbp_create_parser,yara_indexer_create_parser])
	create_parser.set_defaults(which='create')

	# update
	update_parser = subparsers.add_parser('update', help='Update existing clusters and index', parents=[default_parser,default_update_parser,taxsbp_parser,taxsbp_update_parser])
	update_parser.set_defaults(which='update')

	args = parser.parse_args()
	if len(sys.argv[1:])==0: # Print help calling script without parameters
		parser.print_help() 
		return 0

	tx_total = time.time()
	output_folder = args.output_folder + '/'
	# Validations:
	if args.which=='create':
		if not os.path.exists(output_folder): 
			os.makedirs(output_folder) # Create folder if doesn't exist
		elif not os.listdir(output_folder)==[]:
			print("Output folder not empty")
			return 1
	elif args.which=='update':
		# Check first file to define gzip output for update
		args.gzip_output = True if os.path.exists(output_folder+'0.fna.gz') else False
	gz_extension = ".gz" if args.gzip_output else ""
	
	if args.retrieve_ncbi:
		taxsbp_output_folder = output_folder + "taxsbp/"
		if os.path.exists(taxsbp_output_folder): shutil.rmtree(taxsbp_output_folder) # delete if already exists
		os.makedirs(taxsbp_output_folder)
		taxsbp_input_file = retrieve_taxid_ncbi(taxsbp_output_folder, args.input_files, args.threads, args.taxsbp_path)
		taxsbp_nodes_file, taxspb_merged_file = get_taxdump(taxsbp_output_folder)
	else:
		if not args.len_taxid_file or not args.nodes_file or not args.merged_file:
			print("--len-taxid-file --nodes-file --merged-file should be provided or --retrieve-ncbi activated")
			return 1
		else:
			taxsbp_input_file = args.len_taxid_file
			taxsbp_nodes_file = args.nodes_file 
			taxspb_merged_file = args.merged_file

	taxsbp_bins_file = output_folder + 'bins.txt'
	if args.which=='create':
		tx = time.time()
		print("Running taxonomic clustering (TaxSBP)... ", sep='', end='')
		run_taxsbp_cmd = 'python3 {0}TaxSBP.py create -f {1} -n {2} -m {3} -s {4} -{5} {6}'.format(
							args.taxsbp_path,
							taxsbp_input_file, 
							taxsbp_nodes_file, 
							taxspb_merged_file,
							args.start_node,
							'l' if args.bin_length else 'b',
							args.bin_length if args.bin_length else args.number_of_bins)
		_, stderr, errcode = run(run_taxsbp_cmd, taxsbp_bins_file)
		stdout, stderr, errcode = run('tail -n 1 ' + taxsbp_bins_file)
		actual_number_of_bins = int(stdout.split("\t")[-1])+1
		print(actual_number_of_bins, " bins created. ", end='', sep='')
		print("Done. Elapsed time: " + str(time.time() - tx) + " seconds")
	
		tx = time.time()
		print("Writing files... ", sep='', end='')
		taxsbp_write_files(args.input_files, taxsbp_bins_file, args.gzip_output, output_folder)
		print("Done. Elapsed time: " + str(time.time() - tx) + " seconds")
		
		tx = time.time()
		print("Building bloom filter (yara_build_filter)... ", sep='', end='')
		run_yara_build_filter_cmd = '{0}yara_build_filter -v -t {1} -b {2} -k {3} -nh {4} -bs {5} -ft bloom -o {6} {7}'.format(
										args.yara_path,
										args.threads,
										actual_number_of_bins,
										args.kmer_size,
										args.num_hash,
										args.bloom_size,
										output_folder+"bloom.filter",
										output_folder)
		stdout, stderr, errcode = run(run_yara_build_filter_cmd)
		print("Done. Elapsed time: " + str(time.time() - tx) + " seconds")

		tx = time.time()
		print("Building indices (yara_indexer_dis)... ", sep='', end='')
		run_yara_indexer_dis_cmd = '{0}yara_indexer_dis -v -t {1} -b {2} -o {3} {4}'.format(
							args.yara_path,
							args.threads,
							actual_number_of_bins,
							output_folder,
							output_folder)
		stdout, stderr, errcode = run(run_yara_indexer_dis_cmd)
		print("Done. Elapsed time: " + str(time.time() - tx) + " seconds")
		
	elif args.which=='update':
		# Create tmp folder to work with updated indices
		tmp_output_folder = output_folder + "tmp/"
		if os.path.exists(tmp_output_folder): shutil.rmtree(tmp_output_folder) # delete if already exists
		os.makedirs(tmp_output_folder)

		taxsbp_updated_bins_file = tmp_output_folder + 'updated_bins.txt' # New file after removing bins
		if args.remove_file: # If there are files to be removed

			tx = time.time()
			print("Removing files from bins... ", sep='', end='')
			# First check which bins will be modified
			run_taxsbp_cmd = 'python3 {0}TaxSBP.py list -f {1} -i {2} '.format(
							args.taxsbp_path,
							args.remove_file, 
							taxsbp_bins_file)
			stdout, stderr, errcode = run(run_taxsbp_cmd)
			
			# list bins affected by removal and put to a set (to make it uniq)
			affected_rem_bins = set([line.split("\t")[3] for line in stdout.splitlines()])
			print(len(affected_rem_bins), " bins affected. ", sep='', end='')
			
			# make updated bin list without removed files
			run_taxsbp_cmd = 'python3 {0}TaxSBP.py remove -f {1} -i {2} '.format(
							args.taxsbp_path,
							args.remove_file, 
							taxsbp_bins_file)
			_, stderr, errcode = run(run_taxsbp_cmd, taxsbp_updated_bins_file)

			# Re-write file with removed sequences into tmp folder
			taxsbp_write_files([output_folder+str(ab)+".fna"+gz_extension for ab in affected_rem_bins], taxsbp_updated_bins_file, args.gzip_output, tmp_output_folder)
			print("Done. Elapsed time: " + str(time.time() - tx) + " seconds")

		else:
			affected_rem_bins = set() # make it empty for further comparison
			# No entries removed, copy file current bins.txt to new file
			shutil.copyfile(taxsbp_bins_file, taxsbp_updated_bins_file)

		# TaxSBP add new sequences
		tx = time.time()
		print("Adding files to bins... ", sep='', end='')
		taxsbp_added_bins_file = tmp_output_folder + 'added_bins.txt' # New file for the added bins defintion
		run_taxsbp_cmd = 'python3 {0}TaxSBP.py add -f {1} -i {2} -n {3} -m {4} {5}'.format(
						args.taxsbp_path,
						taxsbp_input_file, 
						taxsbp_updated_bins_file,
						taxsbp_nodes_file, 
						taxspb_merged_file,
						"--distribute" if args.distribute else "")
		_, stderr, errcode = run(run_taxsbp_cmd, taxsbp_added_bins_file)

		# list bins affected by addition and put to a set (to make it uniq)
		affected_add_bins = set([line.rstrip().split("\t")[3] for line in open(taxsbp_added_bins_file,'r')])
		print(len(affected_add_bins), " bins affected. ", sep='', end='')
		
		# Copy bins which sequences will be added (skipping the ones already there from removing sequences)
		for ab in affected_add_bins-affected_rem_bins:
			shutil.copyfile(output_folder+str(ab)+".fna"+gz_extension, tmp_output_folder+str(ab)+".fna"+gz_extension)

		# append new sequences to the copied files 
		taxsbp_write_files(args.input_files, taxsbp_added_bins_file, args.gzip_output, tmp_output_folder)
		print("Done. Elapsed time: " + str(time.time() - tx) + " seconds")

		tx = time.time()
		print("Updating bloom filter (yara_update_filter)... ", sep='', end='')
		# copy bloom filer to tmp
		shutil.copyfile(output_folder+"bloom.filter",tmp_output_folder+"bloom.filter")
		run_yara_update_filter_cmd = '{0}yara_update_filter -v -t {1} -ft bloom {2} {3}'.format(
										args.yara_path,
										args.threads,
										tmp_output_folder+"bloom.filter",
										' '.join([tmp_output_folder+str(ab)+".fna"+gz_extension for ab in affected_add_bins-affected_rem_bins]))
		stdout, stderr, errcode = run(run_yara_update_filter_cmd)
		print("Done. Elapsed time: " + str(time.time() - tx) + " seconds")

		tx = time.time()
		print("Building indices (yara_indexer)... ", sep='', end='')
		run_yara_indexer_cmd = "parallel --gnu -j {0} '{1}yara_indexer -v -o {{}} {{}}{2}' ::: {3}".format(
							args.threads,
							args.yara_path,
							".fna"+gz_extension,
							' '.join([tmp_output_folder+str(ab) for ab in affected_add_bins-affected_rem_bins]))
		stdout, stderr, errcode = run(run_yara_indexer_cmd)
		print("Done. Elapsed time: " + str(time.time() - tx) + " seconds")

		# if successful, mv tmp files over originals, join updated and added over bins.txt, remove tmp folder
		# Overwrite added/updated bins to the bins file and remove files
		tx = time.time()
		print("Moving new files... ", sep='', end='')
		with open(taxsbp_bins_file, 'w') as file:
		 	file.write(open(taxsbp_updated_bins_file,'r').read())
		 	file.write(open(taxsbp_added_bins_file,'r').read())
		os.remove(taxsbp_updated_bins_file)
		os.remove(taxsbp_added_bins_file)
		# Move new indices and bloom to main folder
		for file in os.listdir(tmp_output_folder):
			shutil.move(tmp_output_folder+file,output_folder+file)
		# Delte empty tmp folder
		os.rmdir(tmp_output_folder)
		print("Done. Elapsed time: " + str(time.time() - tx) + " seconds")

	# Delete taxsbp files
	if args.retrieve_ncbi: shutil.rmtree(taxsbp_output_folder)
	print("Total elapsed time: " + str(time.time() - tx_total) + " seconds")

def run(cmd, output_file=None):
	try:
		errcode=0
		process = subprocess.Popen(shlex.split(cmd), shell=False, universal_newlines=True, stdout=open(output_file, 'w') if output_file else subprocess.PIPE, stderr=subprocess.PIPE)	
		stdout, stderr = process.communicate() # wait for the process to terminate
		errcode = process.returncode
		if errcode!=0: raise Exception()
		return stdout, stderr, errcode
	#except OSError as e: # The most common exception raised is OSError. This occurs, for example, when trying to execute a non-existent file. Applications should prepare for OSError exceptions.
	#except ValueError as e: #A ValueError will be raised if Popen is called with invalid arguments.
	except Exception as e:
		print('The following command failed to execute:\n'+cmd)
		print(e)
		print("Errorcode: "+str(errcode))
		print("Error:\n"+stderr)
		raise

def taxsbp_write_files(files, taxsbp_bins_file, gzip_output, output_folder):

	### TODO - double check appending on files, remove them first if possible (specially tmp) ###
	bins = {}
	with open(taxsbp_bins_file, 'r') as file:
		for line in file:
			seqid, _, _, binid = line.rstrip().split("\t")
			bins[seqid] = binid

	for file in files:
		handler = gzip.open(file, 'rt') if file.endswith(".gz") else open(file,'r')
		for header, sequence in fasta_read_generator(handler):
			if header.split(' ', 1)[0] in bins:
				outfilename = output_folder + bins[header.split(' ', 1)[0]] + ".fna"
				outfile = gzip.open(outfilename+".gz", 'at') if gzip_output else open(outfilename, 'a')
				outfile.write('>' + header + '\n' + sequence)
				outfile.close()

def fasta_read_generator(file_handler):
    """
    This function returns a generator that yields name (without the leading >) and sequence tuples from a fasta file of
    which the handler is given.
    
    :param file_handler: file object wrapper for the input fasta file 
    :return: generator yielding name, sequence tuples 
    """
    seq = []
    name = ''
    for line in file_handler:
        if line[0] == '>':
            sequence = ''.join(seq)
            if name:  # only yield when we already have all data for the first sequence
                yield name, sequence
            name = line.rstrip()[1:]  # omitting the leading >
            seq = []
        else:
            seq += [line]#.rstrip()] # keep line breaks
    sequence = ''.join(seq)
    yield name, sequence  # don't forget the last sequence

def get_taxdump(taxsbp_output_folder):
	tx = time.time()
	print("Downloading taxdump... ", sep='', end='')
	taxdump_file = taxsbp_output_folder+'taxdump.tar.gz'
	run_wget_taxdump_cmd = 'wget -qO {0} ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz'.format(taxdump_file)
	stdout, stderr, errcode = run(run_wget_taxdump_cmd)
	unpack_taxdump_cmd = 'tar xf {0} -C "{1}" nodes.dmp merged.dmp'.format(taxdump_file, taxsbp_output_folder)
	stdout, stderr, errcode = run(unpack_taxdump_cmd)
	print("Done. Elapsed time: " + str(time.time() - tx) + " seconds")
	return taxsbp_output_folder+'nodes.dmp', taxsbp_output_folder+'merged.dmp'

def retrieve_taxid_ncbi(taxsbp_output_folder, files, threads, taxsbp_path):
	tx = time.time()
	print("Extracting accessions... ", sep='', end='')
	ids = []
	for file in files:
		handler = gzip.open(file, 'rt') if file.endswith(".gz") else open(file,'r')
		for header, sequence in fasta_read_generator(handler):
			ids.append(header.split(' ', 1)[0])
	f=open(taxsbp_output_folder + 'accessions.txt','w')
	f.write('\n'.join(ids))
	f.close()
	print("Done. Elapsed time: " + str(time.time() - tx) + " seconds")

	tx = time.time()
	print("Retrieving data from NCBI... ", sep='', end='')
	taxsbp_input_file = taxsbp_output_folder + 'acc_len_taxid.txt'
	run_get_len_taxid_cmd = 'parallel --gnu -j {0} -a {1} {2}get_len_taxid.sh {{}}'.format(
							threads,
							taxsbp_output_folder + 'accessions.txt',
							taxsbp_path)
	_, stderr, errcode = run(run_get_len_taxid_cmd, taxsbp_input_file)
	if stderr: print(stderr) # print possible failed ids
	print("Done. Elapsed time: " + str(time.time() - tx) + " seconds")
	return taxsbp_input_file

if __name__ == '__main__':
	main()
